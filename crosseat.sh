#!/bin/bash
#Bu script GPL3 ile özgürce kullanılabilir.
go_crash(){
	echo -e "$*"
	exit 1
}

clear
go_start() {
	#Temel değişkenleri ayarla
	source ./ayarlar.conf
	touch ./score
	ekran=`tput cols`
	declare -g genislik=`echo $ekran-1 | bc`
	declare -g konum=`echo $genislik/2 | bc`
	declare -g kalan=`echo $final-$seviye | bc`
	declare -g kalan_sbt=${kalan}
	declare -g seviye_sbt=${seviye}
	declare -g username_stck=${USER}
	declare -g user_name=${USER}
	declare -g skor=0
	declare -ig debug_switch=0
	declare -ag ileri[0]="|"

	for i in `seq 0 ${genislik}`
	do
		ileri[$i]='|'
	done

	which bc &>/dev/null || go_crash "Lütfen 'bc' paketini yükleyin."
	which tput &>/dev/null || go_crash "Lütfen 'ncurses' paketini yükleyin."
}

go_interface() {
	#Arayüz iskeleti
	clear
	declare -ag ayirici
	for i in `seq ${genislik}`
	do
        	ayirici+==
	done

	echo ${ayirici[@]}
	echo CROSSEAT | figlet 2>/dev/null || echo CROSSEAT
	echo ${ayirici[@]}
	echo En İşsizler
	echo
	cat ./score | sort -nr | head -n  10
	echo ${ayirici[@]}
	read -p "Bir isim yaz: " user_name
	clear
	echo ${ayirici[@]}
	echo
	echo Yön tuşları: Sağ-Sol
	echo a: sola hareket
	echo d: sağa hareket
	echo r: reset
	echo t: hata ayıklama verilerini aç/kapat
	echo y: ayarlar
	echo q: çıkış
	echo
	echo ${ayirici[@]}
	sleep 2
}


go_fill() {
	#Satırları set verileri ile doldur
	declare -ag sifirinci_satir=("-")
	declare -ag birinci_satir=("-")
        declare -ag ikinci_satir=("-")
        declare -ag ucuncu_satir=("-")
        declare -ag dorduncu_satir=("-")
        declare -ag besinci_satir=("-")
        declare -ag altinci_satir=("-")
        declare -ag yedinci_satir=("-")
        declare -ag sekizinci_satir=("-")
        declare -ag dokuzuncu_satir=("-")
        declare -ag onuncu_satir=("-")
	declare -ag taban=("${zemin}")
	
	for i in `seq 0 10`
	do
		declare -Ag puan[$i]=$i
	done
	
	for i in `seq 0 10`
	do
		declare -ig puan_col$i=0
	done

	for i in `seq 0 ${genislik}`
        do
                sifirinci_satir[i]='-'
                birinci_satir[i]='-'
                ikinci_satir[i]='-'
                ucuncu_satir[i]='-'
		dorduncu_satir[i]='-'
                besinci_satir[i]='-'
                altinci_satir[i]='-'
                yedinci_satir[i]='-'
                sekizinci_satir[i]='-'
		dokuzuncu_satir[i]='-'
                onuncu_satir[i]='-'
		taban[i]=${zemin}
        done
}

go_write_lines() {
	#Değerleri ekrana bas
	clear
#	echo ${sifirinci_satir[@]} | tr -d ' '
	echo ${birinci_satir[@]} | tr -d ' '
        echo ${ikinci_satir[@]} | tr -d ' '
        echo ${ucuncu_satir[@]} | tr -d ' '
        echo ${dorduncu_satir[@]} | tr -d ' '
        echo ${besinci_satir[@]} | tr -d ' '
        echo ${altinci_satir[@]} | tr -d ' '
        echo ${yedinci_satir[@]} | tr -d ' '
        echo ${sekizinci_satir[@]} | tr -d ' '
        echo ${dokuzuncu_satir[@]} | tr -d ' '
        echo ${taban[@]} | tr -d ' '
	echo ${ileri[@]} | tr -d ' ' 


	if [ 1 -eq ${debug_switch} ]
	then
		go_debug
	fi

	echo "${skor}"| figlet 2>/dev/null || echo "${skor} - ${kalan}"

}

go_debug() {
	# Koşturma sırasında hatalar olduğunu düşünüyorsanız
	# ya da bi' şeyleri kırıp döktüyseniz debug değerlerini
	# takip ederek düzeltmeler yapabilirsiniz.
	
        echo
	echo -e "Kullanıcı: ${user_name} \t Skor: ${skor}"
        echo Karakter: $karakter
        echo Karakter konumu: $konum \/ $genislik
        echo Zemin: $zemin
        echo Seviye: $seviye \/ $kalan
        echo Dolgu: "-"
        echo Final: $final
        echo Kalan adım sayısı: $kalan
        echo -e "Genişlik: ${genislik} \t Ekran: ${ekran} \t Aralık: ${orta}" #Ekran, terminal genişliği; Genişlik, kırpılmış ölçü.
	echo
	echo -e "Index \t         Puan \t         Puan_Index"
        echo -e "S00: ${#sifirinci_satir[@]} \t PS00: ${puan[0]} \t ${puan_col0}" #S00:Dizideki index sayısı;PS00:Satırdaki Puan değeri ve Puanın bulunduğu index numarası.
        echo -e "S01: ${#birinci_satir[@]} \t PS01: ${puan[1]} \t ${puan_col1}"
        echo -e "S02: ${#ikinci_satir[@]} \t PS02: ${puan[2]} \t ${puan_col2}"
        echo -e "S03: ${#ucuncu_satir[@]} \t PS03: ${puan[3]} \t ${puan_col3}"
        echo -e "S04: ${#dorduncu_satir[@]} \t PS04: ${puan[4]} \t ${puan_col4}"
        echo -e "S05: ${#besinci_satir[@]} \t PS05: ${puan[5]} \t ${puan_col5}"
        echo -e "S06: ${#altinci_satir[@]} \t PS06: ${puan[6]} \t ${puan_col6}"
        echo -e "S07: ${#yedinci_satir[@]} \t PS07: ${puan[7]} \t ${puan_col7}"
        echo -e "S08: ${#sekizinci_satir[@]} \t PS08: ${puan[8]} \t ${puan_col8}"
        echo -e "S09: ${#dokuzuncu_satir[@]} \t PS09: ${puan[9]} \t ${puan_col9}"
        echo -e "S10: ${#onuncu_satir[@]} \t PS10: ${puan[10]} \t ${puan_col10}"
	echo

}

go_change(){
	#Değerleri değiştir veye alta kaydır
	for i in `seq 0 ${genislik}`
	do
		taban[$i]=${zemin}
	done	
	taban[$konum]=${karakter}
	
	for i in `seq 0 ${genislik}`
	do
		onuncu_satir[$i]=${dokuzuncu_satir[$i]}
		dokuzuncu_satir[$i]=${sekizinci_satir[$i]}
		sekizinci_satir[$i]=${yedinci_satir[$i]}
		yedinci_satir[$i]=${altinci_satir[$i]}
		altinci_satir[$i]=${besinci_satir[$i]}
		besinci_satir[$i]=${dorduncu_satir[$i]}
		dorduncu_satir[$i]=${ucuncu_satir[$i]}
		ucuncu_satir[$i]=${ikinci_satir[$i]}
		ikinci_satir[$i]=${birinci_satir[$i]}
		birinci_satir[$i]=${sifirinci_satir[$i]}

		sifirinci_satir[$i]="-"

	done
	
	puan[10]=${puan[9]}
	puan[9]=${puan[8]}
	puan[8]=${puan[7]}
	puan[7]=${puan[6]}
	puan[6]=${puan[5]}
	puan[5]=${puan[4]}
	puan[4]=${puan[3]}
	puan[3]=${puan[2]}
	puan[2]=${puan[1]}
	puan[1]=${puan[0]}

	puan_col10=${puan_col9}	
	puan_col9=${puan_col8}	
	puan_col8=${puan_col7}	
	puan_col7=${puan_col6}	
	puan_col6=${puan_col5}	
	puan_col5=${puan_col4}	
	puan_col4=${puan_col3}	
	puan_col3=${puan_col2}	
	puan_col2=${puan_col1}	
	puan_col1=${puan_col0}
}

go_goal(){
	#Ödülleri yaratmak
	genlik=`echo $genislik -10 | bc`
	orta=`echo $genlik / 2 - ${seviye} / 2 | bc`
	puan[0]=`echo $RANDOM % 10 | bc`
	puan_col0=`echo $orta + $RANDOM % $seviye | bc`
	sifirinci_satir[$puan_col0]=${puan[0]}

	#Ödülü almak
	if [ ${puan_col10} -eq ${konum} ]
	then
		echo Kaptın ${puan[10]} puanı ${user_name}
		skor=`echo $skor + ${puan[10]} | bc`
		go_hard
	fi
}

go_hard() {
	#Puan kazandıkça seviye 1 artsın.
	seviye=`echo ${seviye} + 1 | bc`
	if [ ${seviye} -gt ${final} ]
        then
                go_quit
        fi

	kalan=`echo ${final}-${seviye} | bc`

	#İlerleme çubuğu
	for i in `seq 0 ${genislik}`
	do
		ileri[$i]="-"
	done

	ilerleme=`echo "${genislik} / ${kalan_sbt} * ${kalan}" | bc`
	for i in `seq 0 ${ilerleme}`
	do
		ileri[$i]='|'
	done
}

go_quit(){
	#Çıkış prosedürü
	echo -e "$skor \t $user_name" >> ./score
	clear
	echo " ${user_name}  :  $skor  " | figlet 2>/dev/null || echo "${user_name}: ${skor}"

	echo Diğer işsizler:
	echo
	cat ./score | sort -nr | head -n 10
	sleep 2
	exit 0
}

go_left(){
	#Sola git
	konum=`echo $konum-1 | bc`
	go_change
	go_write_lines
	go_goal
}

go_right(){
	#Sağa git
	konum=`echo $konum+1 | bc`
	go_change
	go_write_lines
	go_goal
}
#İnit işlemleri
go_start
go_interface #Bu satırı deaktif ederseniz doğrudan oyuna başlar
go_fill
go_change
go_write_lines

## Tuşların okunması
while true
do
        read -r -sn1 key
        case $key in
        D)
                go_left
        ;;
        a)
                go_left
        ;;
        C)
                go_right
        ;;
        d)
                go_right
        ;;
        q)
                go_quit
        ;;
        r)
                clear
		username_stck=${user_name}
                go_start
		user_name=${username_stck}
		go_fill
		go_change
		go_write_lines

        ;;
        t)
		if [ 1 -eq ${debug_switch} ]
		then
			debug_switch=0
		else
			debug_switch=1
		fi
	;;
	y)

		sh -c "eval '$EDITOR ./ayarlar.conf'; echo -e 'Yeni ayarların etkinleşmesi için _r_ tuşuna basın.\nİsterseniz yeniden başlatmadan da oyuna devam edebilirsiniz.'; sleep 1"
	
	esac
	
done

exit 0

